package fileutil

import (
	"context"
	"io"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

func Copy(ctx context.Context, src, dst string, fileMode os.FileMode) error {
	if ctx.Err() != nil {
		return ctx.Err()
	}

	srcFile, err := os.Open(src)
	if err != nil {
		return errors.Wrapf(err, "failed to open file %q", src)
	}
	defer srcFile.Close()

	if err = os.MkdirAll(filepath.Dir(dst), fileMode); err != nil {
		return errors.Wrapf(err, "failed to create directory %q", filepath.Dir(dst))
	}

	tempDst := dst + ".part"

	tempDstFile, err := os.Create(tempDst)
	if err != nil {
		return errors.Wrapf(err, "failed to create temp file %q", tempDst)
	}
	defer tempDstFile.Close()

	if _, err = io.Copy(tempDstFile, srcFile); err != nil {
		return errors.Wrap(err, "failed to copy file")
	}

	if err = tempDstFile.Sync(); err != nil {
		return errors.Wrapf(err, "failed to sync temp file %q", tempDst)
	}

	if err = tempDstFile.Chmod(fileMode); err != nil {
		return errors.Wrapf(err, "failed to chmod temp file %q", tempDst)
	}

	if err = os.Rename(tempDst, dst); err != nil {
		return errors.Wrapf(err, "failed to rename temp file %q", tempDst)
	}

	return nil
}

func CopyDir(ctx context.Context, src, dst string, fileMode os.FileMode) error {
	err := filepath.Walk(src, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return errors.Wrap(err, "failed to access path")
		}

		if info.IsDir() {
			return nil
		}

		relativePath, err := filepath.Rel(src, path)
		if err != nil {
			return errors.Wrapf(err, "failed to determine relative path for %q", path)
		}

		fileSrc := path
		fileDst := filepath.Join(dst, relativePath)

		err = Copy(ctx, fileSrc, fileDst, fileMode)
		return errors.Wrapf(err, "failed to copy %q to %q", fileSrc, fileDst)
	})

	return errors.Wrapf(err, "failed to walk %q", src)
}
