package fileutil

import (
	"strings"

	"github.com/pkg/errors"
)

var (
	ErrFilenameEmpty     = errors.New("filename is empty")
	ErrSafeFilenameEmpty = errors.New("safe filename is empty")
)

func SafeFilename(filename string) (string, error) {
	if len(filename) == 0 {
		return "", ErrFilenameEmpty
	}

	allowedCharacters := strings.Replace(filename, "/", "", -1)
	allowedCharacters = strings.Replace(allowedCharacters, "\\0", "", -1)

	lastPeriodIndex := strings.LastIndex(allowedCharacters, ".")
	for len(allowedCharacters) > 0 && lastPeriodIndex == len(allowedCharacters)-1 {
		allowedCharacters = allowedCharacters[:lastPeriodIndex]
		lastPeriodIndex = strings.LastIndex(allowedCharacters, ".")
	}

	trimmed := strings.TrimSpace(allowedCharacters)
	if len(trimmed) == 0 {
		return "", ErrSafeFilenameEmpty
	}

	return trimmed, nil
}
