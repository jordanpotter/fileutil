package fileutil

import (
	"testing"
)

func TestSafeFilename(t *testing.T) {
	tests := []struct {
		filename    string
		expectedStr string
		expectedErr error
	}{
		{"", "", ErrFilenameEmpty},
		{".", "", ErrSafeFilenameEmpty},
		{"/\\0", "", ErrSafeFilenameEmpty},

		{"hello", "hello", nil},
		{"helló", "helló", nil},
		{".hello.", ".hello", nil},
		{"hello.", "hello", nil},
		{"hello...", "hello", nil},
		{" hello ", "hello", nil},
		{"*hello*", "*hello*", nil},
		{"hello/there", "hellothere", nil},
		{"hello\\0there", "hellothere", nil},
		{"hello (there)", "hello (there)", nil},
		{"hello & there!", "hello & there!", nil},
	}

	for _, test := range tests {
		result, err := SafeFilename(test.filename)
		if test.expectedErr != err {
			t.Errorf("expected error %v, got %v", test.expectedErr, err)
		} else if test.expectedStr != result {
			t.Errorf("expected %v, got %v", test.expectedStr, result)
		}
	}
}
