package fileutil

import (
	"context"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

func Move(ctx context.Context, src, dst string, fileMode os.FileMode) error {
	if err := os.MkdirAll(filepath.Dir(dst), fileMode); err != nil {
		return errors.Wrapf(err, "failed to create directory %q", filepath.Dir(dst))
	}

	if err := os.Rename(src, dst); err != nil {
		return errors.Wrapf(err, "failed to move file %q to %q", src, dst)
	}

	if err := os.Chmod(dst, fileMode); err != nil {
		return errors.Wrapf(err, "failed to chmod file %q", dst)
	}

	return nil
}
